<?php

use yii\helpers\Html;

echo "<h1>Ejercicio 2</h1>";

echo "<h3>Con foreach</h3>";
echo "<ul class='list-group list-group-horizontal'>";
foreach ($imagenes as $imagen) {
    echo "<li class='list-group-item col-lg-3'>";
    echo Html::img("@web/imgs/{$imagen}", [
        'class' => 'img-thumbnail',
    ]);
    echo "</li>";
}
echo "</ul>";


