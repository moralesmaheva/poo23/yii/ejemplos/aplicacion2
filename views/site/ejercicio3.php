<?php

use yii\helpers\Html;

echo "<h1>Ejercicio 3</h1>";

//Sin utilizar los helpers
?>

<div class="row">
    <div class="card">
        <div class="col-sm-3">
            <img src="<?= Yii::getAlias("@web") ?>/imgs/<?= $alumno["imagen"] ?>">
            <div class="card-body">
                <p> Nombre: <?= $alumno["nombre"] ?></p>
                <p>Poblacion: <?= $alumno["poblacion"] ?></p>
            </div>
        </div>
    </div>
</div>




<?php

//Utilizando helpers	

?>

<div class="row">
    <div class="col-sm-3">
        <div class="card">
            <?= Html::img("@web/imgs/" . $alumno["imagen"], ["class" => "card-img-top"]) ?>
            <?= Html::ul($alumnoSinFoto, [
                "class" => "list-group list-group-flush",
                "itemOptions" => [
                    "class" => "list-group-item"
                ],
                // "item" => function ($dato, $indice) {
                //     $salida = "";
                //     if ($indice != "imagen") {
                //         $salida .= "<li class='list-group-item'>{$indice}: {$dato}</li>";
                //     }
                //     return $salida;
                // }

            ]) ?>
        </div>
    </div>
</div>