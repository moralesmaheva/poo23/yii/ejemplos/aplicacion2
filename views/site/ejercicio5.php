<?php

// $salida = "<div class='row'>";
// $salida .= "<div class='col-sm-6'>";
// foreach ($datos as $indice => $dato) {
//     $salida .= "<h5 class='card-title'>" . $dato["id"] . ".</h5>";
//     $salida .= "<p class='card-text'>" . $dato["nombre"] . "</p>";
//     $salida .= "<ul>";
//     foreach ($dato as $indice => $valor) {
//         if ($indice != "id" && $indice != "nombre") {
//             $salida .= "<li class='list-group-item'>" . $indice . ": " . $valor . "</li>";
//         }
//         $salida .= "</ul>";
//     }
// }
// $salida .= "</div>";
// $salida .= "</div>";

// echo $salida;


//opcion 2
?>
<div class="row">
    <?php
    foreach ($datos as $dato) {
    ?>
        <div class="col-sm-3">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Id: <?= $dato["id"] ?></h3>
                </div>
                <div class="card-body">
                    <p class="card-text">Nombre: <?= $dato["nombre"] ?></p>
                </div>
                <ul class=" list-group list-group-flush">
                    <li class="list-group-item">Poblacion: <?= $dato["poblacion"] ?></li>
                    <li class="list-group-item">Direccion: <?= $dato["direccion"] ?></li>
                </ul>
            </div>
        </div>
    <?php
    }
    ?>
</div>
