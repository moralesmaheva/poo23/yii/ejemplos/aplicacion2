<?php

use yii\helpers\Html;

echo "<h1>Ejercicio 4</h1>";


 // con el helper ul
echo Html::ul($numeros, [
    "class" => "list-group list-group-horizontal",
    "itemOptions" => [
        "class" => "list-group-item",
    ],
]);

echo "<br>";
echo "<hr>";
echo "<br>";

//renderizando todos los elementos
//con subvistas
echo $this->render('_listado', [
    'numeros' => $numeros
]);

echo "<br>";
echo "<hr>";
echo "<br>";

//sin cargar vistas
?>

<ul class="list-group list-group-horizontal">
    <?php
    foreach ($numeros as $numero) {
        echo '<li class="list-group-item">' . $numero . '</li>';
    }
    ?>
</ul>