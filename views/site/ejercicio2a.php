<?php

use yii\helpers\Html;

echo "<h1>Ejercicio 2</h1>";

echo "<h3>Con Helper ul</h3>";
echo Html::ul(
    $imagenes,
    [
        'class' => 'list-group list-group-horizontal',
        "item" => function ($imagen) {
            $salida = "<li class='list-group-item col-lg-3'>";
            $salida .= Html::img("@web/imgs/{$imagen}",[
                    'class' => 'img-thumbnail',
                ]
            );
            $salida .= "</li>";
            return $salida;
        },
    ]
);
