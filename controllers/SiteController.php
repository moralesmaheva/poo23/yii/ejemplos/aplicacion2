<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionEjercicio1()
    {
        $alumnos = [
            'Miguel',
            'Maria',
            'Pedro',
            'Samuel',
        ];

        return $this->render('ejercicio1', ['alumnos' => $alumnos]);
    }



    public function actionEjercicio2b()
    {
        $fotos = [
            'alumno1.png',
            'alumno2.png',
            'alumno3.png',
            'alumno4.png',
        ];
        return $this->render('ejercicio2b', ['imagenes' => $fotos]);
    }

    public function actionEjercicio2a()
    {
        $fotos = [
            'alumno1.png',
            'alumno2.png',
            'alumno3.png',
            'alumno4.png',
        ];
        return $this->render('ejercicio2a', ['imagenes' => $fotos]);
    }

    public function actionEjercicio3($id = 0)
    {
        $alumnos = [
            [
                "nombre" => "Miguel",
                "poblacion" => "Santander",
                "imagen" => "alumno1.png"
            ],
            [
                "nombre" => "Maria",
                "poblacion" => "Laredo",
                "imagen" => "alumno2.png"
            ],
            [
                "nombre" => "Pedro",
                "poblacion" => "Cartes",
                "imagen" => "alumno3.png"
            ],
            [
                "nombre" => "Samuel",
                "poblacion" => "Santander",
                "imagen" => "alumno4.png"
            ],
        ];
        //quito la imagen en el array alumno para utilizarlo en el widget
        $alumnoSinFoto = $alumnos[$id];
        unset($alumnoSinFoto["imagen"]);
        return $this->render('ejercicio3', [
            "alumno" => $alumnos[$id],
            "alumnoSinFoto" => $alumnoSinFoto
        ]);
    }

    public function actionEjercicio4()
    {
        $numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        return $this->render('ejercicio4', ['numeros' => $numeros]);
    }

    public function actionEjercicio5()
    {
        $datos = [
            [
                "id" => "1",
                "nombre" => "Miguel",
                "poblacion" => "Santander",
                "direccion" => "Calle 1",
            ],
            [
                "id" => "2",
                "nombre" => "Maria",
                "poblacion" => "Laredo",
                "direccion" => "Calle 2",
            ],
            [
                "id" => "3",
                "nombre" => "Pedro",
                "poblacion" => "Cartes",
                "direccion" => "Calle 3",
            ],
            [
                "id" => "4",
                "nombre" => "Samuel",
                "poblacion" => "Santander",
                "direccion" => "Calle 4",
            ],
        ];
        //quiero que en vista se muestren los datos anteriores
        //utilizando cards
        //en el titulo de la card se coloca el id
        //en el texto de la card se coloca el nombre
        //el resto de los campos en una lista dentro de la card
        //con el formato NombreValor:Valor

        return $this->render('ejercicio5', ['datos' => $datos]);
    }


    public function actionEjercicio6()
    {
        $datos = [
            [
                "id" => "1",
                "nombre" => "Miguel",
                "poblacion" => "Santander",
                "direccion" => "Calle 1",
            ],
            [
                "id" => "2",
                "nombre" => "Maria",
                "poblacion" => "Laredo",
                "direccion" => "Calle 2",
            ],
            [
                "id" => "3",
                "nombre" => "Pedro",
                "poblacion" => "Cartes",
                "direccion" => "Calle 3",
            ],
            [
                "id" => "4",
                "nombre" => "Samuel",
                "poblacion" => "Santander",
                "direccion" => "Calle 4",
            ],
        ];
        //quiero que en vista se muestren los datos anteriores
        //utilizando cards
        //en el titulo de la card se coloca el id
        //en el texto de la card se coloca el nombre
        //el resto de los campos en una lista dentro de la card
        //con el formato NombreValor:Valor
        //utilizando subvistas (realizar el render de la vista)

        return $this->render('ejercicio6', ['datos' => $datos]);
    }
}
